import glob
import json
import logging
import os
import sys
from collections import Iterable

import requests
import time
from flask import Flask, render_template, request
from flask import redirect, flash, session, send_from_directory
from multiprocessing import Process

from utils.products_parser import PriceParser

CURRENT_DIR = os.path.dirname(os.path.realpath(__file__))
UPLOAD_FOLDER = os.path.join(CURRENT_DIR, 'tmp')
ALLOWED_EXTENSIONS = {'xls'}

CONFIG_FILE = os.path.join(UPLOAD_FOLDER, 'config')

app = Flask(__name__)

app.secret_key = 'Nhuih9(*OYgyV&p8FIYVrxYDCP*yfVCYUkuvYRCv&(IUGBu^UT&Y(Gv&CYTUYFGB78FGP(*&IvbiuGV*wt9o'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SESSION_TYPE'] = 'filesystem'

# logging
logger = logging.getLogger('logger')
logger.setLevel(logging.DEBUG)

fh = logging.FileHandler(os.path.join(CURRENT_DIR, 'log'))
fh.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
fh.setFormatter(formatter)
ch.setFormatter(formatter)

logger.addHandler(fh)
logger.addHandler(ch)

FORM_VARS = {'price-vika', 'price-grivna',
             'price-euro', 'price-paints'}

URL = '/'


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def parse_excel_files(price_vika, price_grivna,
                      price_euro, price_paints):
    pass


def validate_and_remove_files(request):
    logger.info('start to validate files')

    # config we need for to read prev files.
    config = {}

    files = request.files
    if 'exchange-rate-euro' not in request.form \
            or 'price-type' not in request.form:
        raise ValueError('No price type or exchange rate in form')

    load_old_prices = True if request.form['price-type'] == 'old' else False

    exchange_rate = float(request.form['exchange-rate-euro'])
    exchange_rates = {'euro': exchange_rate, 'grivna': 1}

    logger.info('exchange rate to euro is {}; is load old prices? {}'
                .format(exchange_rate, load_old_prices))

    if load_old_prices:
        logger.info('Loading old prices')

        with open(CONFIG_FILE, 'r') as config_file:
            config = json.loads(config_file.read())
            logger.info('config was loaded')
    else:
        logger.info('Loading new prices')
        if set(files) == FORM_VARS:
            all_files = {}

            logger.info('all of 4 files are present')
            logger.info('starting file checking')
            for form_file_name in files:
                # Warning! Do not confuse variables filename with file_name!
                logger.info('check file {}'.format(form_file_name))

                file = files[form_file_name]
                filename = file.filename

                file_path = os.path.join(app.config['UPLOAD_FOLDER'],
                                         filename)
                # add row to config
                config[form_file_name] = file_path

                if filename == '':
                    logger.info('No selected file')
                    flash('No selected file')

                    return redirect(URL)

                if file and allowed_file(filename):
                    logger.info('file {} is allowed'.format(filename))
                    all_files[filename] = file
                else:
                    logger.info('file {} is not allowed'.format(filename))
                    flash('Not allowed file')

                    return redirect(URL)

            if len(all_files.keys()) == 4:
                filelist = glob.glob(os.path.join(app.config['UPLOAD_FOLDER'],
                                                  "*"))
                logger.info('starting to delete files {}'.format(filelist))
                for file in filelist:
                    os.remove(file)
                    logger.info('file {} was deleted'.format(file))

                logger.info('starting to saving files')
                for filename, file in all_files.items():
                    logger.info('save new file {}'.format(filename))
                    file.save(os.path.join(app.config['UPLOAD_FOLDER'],
                                           filename))
            # write config
            with open(CONFIG_FILE, 'w') as config_file:
                config_json = json.dumps(config)
                config_file.write(config_json)
            logger.info('config file was created {}'.format(config_json))
        else:
            logger.info('files do not present')
            flash('No file part')
            return redirect(URL)

    # parsing products form excel
    try:
        products = [product for form_name, path in config.items() for product in
                    PriceParser(config_name=form_name,
                                exchange_rates=exchange_rates,
                                )
                        .parse(file_path=path)]
    except:
        logger.info('Something wrong with price')
        flash('Что-то не так с прайсом')
        raise
    else:
        # logger.info('Adding products to shop')
        # product_manager = PrestashopApiManager()
        # product_manager.add_products(products)

        return PriceParser.write_csv(products)


def proceed_parsing_request(request):
    resp = validate_and_remove_files(request)
    try:
        file, filename = resp
        return filename
    except:
        return None


@app.route('/', methods=['GET', 'POST'])
def parse_excel():
    if request.method == 'POST':

        if 'password' in request.form:

            if request.form['password'] == "dartem060698":
                session['approve'] = 'true'

            return redirect(URL)
        else:
            csv_filename = proceed_parsing_request(request)

            if csv_filename is not None:
                return send_from_directory(app.config['UPLOAD_FOLDER'],
                                           csv_filename,
                                           attachment_filename='duza.csv')

    if 'approve' in session and session['approve'] == 'true':
        return render_template('parse-excel.html')

    return render_template('secure.html')


# @app.before_request
# def make_session_permanent():
#     session.permanent = True
#     app.permanent_session_lifetime = timedelta(minutes=1)

def index_products():
    while True:
        logger.info('products indexing')

        url = 'https://duza.com.ua/admin123/searchcron.php?full=1&token=bbxe8n1m&id_shop=1'
        requests.get(url)
        logger.info('products indexed')

        time.sleep(60 * 60 * 6)


if __name__ == '__main__':
    Process(target=index_products).start()
    app.run()
