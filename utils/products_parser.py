import csv
import json
import logging

import sys

import os
from itertools import filterfalse

import xlrd

logger = logging.getLogger('logger')


class Product:
    def __init__(self, name, reference,
                 price, brand, categories,
                 description_short, description,
                 image_link):
        self.name = name
        self.reference = reference
        self.active = 1

        self.price = price

        self.brand = brand
        self.categories = categories

        self.description_short = description_short
        self.description = description

        self.image_link = image_link

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, value):
        self._description = 'Произодитель: {}\n\n{}'.format(self.brand, value)

    # @property
    # def category(self):
    #     return self._category
    #
    # @category.setter
    # def category(self, value):
    #     self._category = value.reverse()

    def __repr__(self):
        return '{}(name="{}", reference="{}", ' \
               'price="{}", brand="{}", description_short="{}", ' \
               'description="{}", image_link="{}")' \
            .format(self.__class__.__name__, self.name,
                    self.reference, self.price,
                    self.brand, self.description_short,
                    self.description, self.image_link)

    def __str__(self):
        return '\n{}(' \
               '\n\tname="{}", reference="{}", ' \
               '\n\tprice="{}", brand="{}", ' \
               '\n\tdescription_short="{}", ' \
               '\n\tdescription="{}", ' \
               '\n\timage_link="{}"' \
               '\n)' \
            .format(self.__class__.__name__, self.name,
                    self.reference, self.price,
                    self.brand, self.description_short,
                    self.description, self.image_link)


class PriceParser:
    def __init__(self, config_name, exchange_rates=None, debug=False):
        with open(os.path.join(os.path.dirname(__file__),
                               'parser-conf.json'), 'r') as file:
            configs = json.load(file)

        logger.info('init parser with config for {} rates {}'.format(config_name,
                                                                     exchange_rates))
        self.config_name = config_name
        self.price_config = configs["prices"][config_name]
        self.exchange_rates = exchange_rates

        self.debug = debug
        if debug:
            logger.info('debug mode is on')

        self.products = []

    def parse(self, file_path):
        self.print('parse excel with config {} path to file {}'.format(self.price_config,
                                                                       file_path))
        workbook = xlrd.open_workbook(file_path)
        self.print('read workbook')

        for index, sheet_config in self.price_config["sheets"].items():
            self.print('trying to get sheet with index {} for {}'
                       .format(index, self.config_name))

            sheet = workbook.sheet_by_index(int(index))

            start_row = sheet_config["start_row"]
            self.print('ignore first {} rows'.format(start_row))

            for row_index in range(start_row, sheet.nrows):
                cells = sheet.row_values(row_index)
                row = [cell for cell in cells]

                self.print('parse row {}'.format(str(row)))

                # # form category
                # categories = 'Категории, {}'.format(
                #     ', '.join([category.capitalize() for category in
                #                sheet_config["categories"]]))
                #
                # categories += ', {}'.format(self.price_config["root_category"]) \
                #     if "root_category" in self.price_config else ''
                # # maybe refactor it?
                # categories = categories.split(',').reverse()
                #
                categories = ['Категории']

                if "root_category" in self.price_config:
                    categories.append(self.price_config["root_category"]
                                      .capitalize())

                categories += [category.capitalize() for category in
                               sheet_config["categories"]]


                categories.reverse()
                categories = ', '.join(categories)

                product = self.__produce_product(row=row,
                                                 sheet_config=sheet_config,
                                                 categories=categories)
                if product is not None:
                    yield product

    def __produce_product(self, row, sheet_config, categories):
        # hide some rows
        row_f = []
        for index, cell in enumerate(row):
            if "rows_to_hide" in sheet_config and \
                            index not in sheet_config["rows_to_hide"]:
                row_f.append(cell)
                row = row_f
        self.print('after deleting redundant cells {}'.format(str(row)))

        # return None if important cell is empty
        important_columns = self.price_config["important_columns"]
        self.print('checking important columns {}'.format(str(important_columns)))
        self.print('amount of cells {}'.format(len(row)))
        for important_column in important_columns:
            if row[important_column] in (None, ' ', ''):
                self.print('ignore row; important columns are absent for row {}'
                           .format(str(row)))
                return None

        # replace ' ' for None
        row = [cell if cell not in (' ', '') else None for cell in row]
        self.print('after replacing \' \' to None {}'.format(str(row)))

        name, reference, price, brand, \
        description_short, description, image_link, *_ = row
        self.print('unpacking row')

        # some names can contain colons
        name = name.replace('; ', '/').replace(';', '/') \
            .replace('<', '').replace('>', '')

        # crunch
        if isinstance(price, str):
            price = price.replace('грн', '')

        # ignore price id 0
        if price in (0, 0.0):
            return None

        currency = sheet_config["currency"]
        price = float(price) * float(self.exchange_rates[currency])
        price = round(price, 2)

        self.print('after currency exchange and rounding {} in {}'
                   .format(price, currency))

        # id
        # category = sheet_config["category"]

        product = Product(name, reference,
                          price, brand,
                          categories, description_short,
                          description, image_link)

        self.print('final {}'.format(product))

        return product

    @staticmethod
    def write_csv(products, filename='duza.csv', debug=False):
        logger.info('trying to write all products to csv')

        with open(os.path.join(os.path.dirname(__file__),
                               '../tmp/{}'.format(filename)), 'w') as file:
            logger.info('starting write to csv file')
            def log(msg):
                if debug:
                    logger.info(msg)

            for product in products:
                log('trying to write product {} to csv'.format(product.name))

                row = [product.name, product.reference,
                       product.price, product.brand,
                       product.categories, product.description_short,
                       product.description, product.image_link]

                names = ["Наименование", "Артикул", "Цена", "Производитель",
                         "Категория", "Краткое описание", "Описание",
                         "Ссылка на фото"]

                writer = csv.writer(file, delimiter=';')
                writer.writerow(row)

                log('wirting to row successed')

            logger.info('writing to csv finished')

            return file, filename

    def print(self, msg):
        if self.debug:
            logger.info(msg)
